import { Routes, Route } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Home from './pages/Home'
import Product from './pages/Product'
import Login from './pages/LoginGoogle';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/:paramURL" element={<Product/>}/>
      <Route path='/*' element={<Home/>}/>
      <Route path='/login' Component={Login}/>
    </Routes>
  );
}

export default App;
