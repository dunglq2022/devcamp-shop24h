import { Container } from "@mui/material";
import CarouselContent from "./Carousel";
import LastestProduct from "./LastestProducts";

function Contents () {
    return(
        <div>
            <CarouselContent/>
            <Container>
                <LastestProduct/>
            </Container>
        </div>
    )
}
export default Contents;