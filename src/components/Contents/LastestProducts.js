import axios from 'axios';
import { useState, useEffect} from 'react';
import ViewAllProduct from './ViewAll';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import { Link, useParams } from 'react-router-dom';
import { Grid, Pagination, Stack } from '@mui/material';
import { Typography, CardContent, Card, Button, AspectRatio } from '@mui/joy';
import FilterProduct from './FilterProduct';
import { useDispatch, useSelector } from 'react-redux';

function LastestProduct() {
    const [limit, setLimit] = useState('6')
    const {paramURL} = useParams();
    const dispatch = useDispatch();

    const {arrProducts, filterProduct} = useSelector((reduxData) => reduxData.contentReducer);
        
    useEffect(() => {
      let config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: `http://localhost:8000/api/products`
      };
  
      axios
        .request(config)
        .then((response) => {
          dispatch({
            type:'GET_PRODUCTS',
            payload: response.data.products
          });
        })
        .catch((error) => {
          console.log(error);npm 
        });
        }, [limit, dispatch]);

    const handleClickViewAll = () => {
        setLimit(0)
    }

    const [currenPage, setCurrentPage] = useState(1);
    //Tính tổng số trang
    const totalPages = Math.ceil(arrProducts.length / limit)
    
    // Lọc và hiển thị dữ liệu cho trang hiện tại
    const startIndex = (currenPage - 1) * limit;
    const endIndex = Math.min(startIndex + limit, arrProducts.length);
    const currentData = arrProducts.slice(startIndex, endIndex);

    // Xử lý sự kiện chuyển trang
    const handlePageChange = (newPage) => {
      setCurrentPage(newPage);
    };

    //Update arrProduct
    const updateProduct = filterProduct.length > 0 ? filterProduct : currentData;

return (
    <>
    <div role="presentation" className='mt-4'>
      <Breadcrumbs aria-label="breadcrumb">
        <Link 
        style={{
          textDecoration: 'none',
          color: 'black',
          fontWeight: 'bold'
        }}
        to='/'>
        Home
        </Link>
        {paramURL === 'products' ? <Typography sx={{fontWeight: 'bold'}} color="text.primary">Products</Typography> : null}
      </Breadcrumbs>
    </div>
    
    {/* DANH MUC SAN PHAM */}
    <Grid sx={{display: 'flex', mt: 2, mb: 2}}>
        <Grid 
          sx={{
            mt: 1,
            mr: 1
          }}
        >
        <FilterProduct/>
        </Grid>
        <Grid container>
          {updateProduct.map((item, index) => {
            let date = new Date(item.createdAt)
            let day = date.getDate();
            let month = date.getMonth();
            let year = date.getFullYear();
            const numberWithCommas = (number) => {
              return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            };
            const formattedPrice = numberWithCommas(item.buyPrice);
            return(
              <Grid sx={{mt: 1, mb: 1, pr: 1, pl: 1}}
              item
              xs={12} sm={6} md={4}
              key={index}
              >
                <Card sx={{ width: '100%' }}>
                  <div>
                    <Typography level="title-md"
                    sx={{
                      whiteSpace: 'nowrap',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                      maxWidth: '100%'
                    }}>{item.name}</Typography>
                    <Typography level="body-sm">{`ngày ${day} tháng ${month} năm ${year}`}</Typography>
                  </div>
                  <AspectRatio minHeight="250px" maxHeight="350px">
                    <img
                      src={item.imageUrl}
                      loading="lazy"
                      alt=""
                    />
                  </AspectRatio>
                  <CardContent orientation="horizontal">
                    <div>
                      <Typography level="body-xs">Giá sách:</Typography>
                      <Typography fontSize="lg" fontWeight="lg">
                        {`${formattedPrice} VNĐ`}
                      </Typography>
                    </div>
                    <Button
                      variant="solid"
                      size="md"
                      color="primary"
                      aria-label="Explore Bahamas Islands"
                      sx={{ ml: 'auto', alignSelf: 'center', fontWeight: 600 }}
                    >
                      Buy
                    </Button>
                  </CardContent>
                </Card>
              </Grid>
            )
          })}
          <Grid container>
          <Stack spacing={2}>
            <Pagination onChange={handlePageChange} page={currenPage} count={totalPages} color="primary" />
          </Stack>
          </Grid>
          <Grid container justifyContent={'center'}>
            <ViewAllProduct handleClickViewAllProps = {handleClickViewAll}/>
          </Grid>
        </Grid>
    </Grid>
    </>
    );
}

export default LastestProduct;
