import { ListItemDecorator, ListItem, ListItemButton, List, Input} from "@mui/joy";
import {TextField, ListItemText, Button} from "@mui/material";
import { useEffect, useState } from "react";
import Checkbox from '@mui/material/Checkbox';
import { useDispatch, useSelector } from "react-redux";

function FilterProduct () {
    const {arrProducts} = useSelector((reduxData) => reduxData.contentReducer);
    const dispatch = useDispatch();
    const [filterBook, setFilterBook] = useState([
        {
            nameBook: '',
            typeBook: [],
            selectBookSell: [],
            priceMin : 0 ,
            priceMax : 0
        }
    ]);
    const [checked, setChecked] = useState([]);
    const [minPrice, setMinPrice] = useState(0);
    const [maxPrice, setMaxPrice] = useState(0)
    const [selectType, setSelectType] = useState([]);
    const [inputSeach, setInputSeach] = useState('')

    const arrayListTheLoai = [
        'Sách Tài Chính', 'Sách Giáo Dục', 'Sách Sức Khỏe', 'Sách Tâm Lý - Xã Hội'
    ];
    const arrayListSale = [
        'Sách bán chạy', 'Đang giảm giá', 'Hết hàng'
    ];

    
    
    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];
        console.log(currentIndex)
        console.log(newChecked)
    
        if (currentIndex === -1) {
          newChecked.push(value);
        } else {
          newChecked.splice(currentIndex, 1);
        }
    
        setChecked(newChecked);
      };

    const handleValueTypeBook = (value) => () => {
        const currentIndex = selectType.indexOf(value);
        const newChecked = [...selectType];
        
    
        if (currentIndex === -1) {
          newChecked.push(value);
        } else {
          newChecked.splice(currentIndex, 1);
        }
    
        setSelectType(newChecked);
      };

    const handlePriceMin = (e) => {
        setMinPrice(e.target.value)
    }

    const handlePriceMax = (e) => {
        setMaxPrice(e.target.value)
    }

    const handleSeachBook = (e) => {
        setInputSeach(e.target.value)
    }

    useEffect(() => {
        setFilterBook({
            nameBook: inputSeach,
            typeBook: selectType,
            selectBookSell: checked,
            priceMin : minPrice ,
            priceMax : maxPrice
        })
    }, [selectType, checked, minPrice, maxPrice, inputSeach]);


    const handleClickFilter = () => {
            
        const filteredName = arrProducts.filter((product) => {
            let keyWordSeach = filterBook.nameBook
            let regex = new RegExp(keyWordSeach, 'i')
    
            // Kiểm tra Tên Book
            if (keyWordSeach.length > 0) {
                return regex.test(product.name)
            }
            return true; // Trả về true để giữ tất cả sản phẩm nếu không có điều kiện lọc tên
        });
    
        const filtered = filteredName.filter((product) => {
            // Kiểm tra condition cho từng thuộc tính filter
            if (filterBook.typeBook.length > 0 && !filterBook.typeBook.includes(product.type[0].name)) {
                return false;
            }
    
            // Chuyển đổi minPrice và maxPrice thành số nguyên
            const minPriceNumber = parseInt(filterBook.priceMin);
            const maxPriceNumber = parseInt(filterBook.priceMax);
    
            // Kiểm tra Giá sách từ giá min
            if (minPriceNumber > 0 && product.buyPrice < minPriceNumber) {
                return false;
            }
    
            // Kiểm tra Giá sách từ giá max
            if (maxPriceNumber > 0 && product.buyPrice > maxPriceNumber) {
                return false;
            }
            return true;
        });
    
        dispatch({
            type: 'UPDATE_PRODUCTS',
            payload: filtered
        });
    }
    

    return(
        <>
        {/* Khung Filter theo tên */}
            <TextField 
                variant="standard"
                size="small"
                label='Seach Products'
                sx={{mb: 2}}
                onChange={handleSeachBook}
            />


            {/* Khung filter theo thể loại */}
            <List
            size="md"
            variant="outlined"
            sx={{
                maxWidth: 200,
                borderRadius: 'md',
            }}
            >
            <ListItem>
                    <ListItemButton>
                        <ListItemDecorator sx={{fontWeight: 'bold'}}>
                            Thể loại sách
                        </ListItemDecorator>
                    </ListItemButton>
            </ListItem>
            {arrayListTheLoai.map((value) => {
                    const labelId = `checkbox-list-secondary-label-${value}`
                    return(
                        <ListItem key={value}>
                            <Checkbox
                                edge="end"
                                onChange={handleValueTypeBook(value)}
                                checked={selectType.indexOf(value) !== -1}
                                inputProps={{ 'aria-labelledby': labelId }}
                                sx={{mr: 1}}
                            ></Checkbox>
                            <ListItemButton>
                            <ListItemText id={labelId} primary={value} />
                            </ListItemButton>
                        </ListItem>
                    )
                })}
            </List>


            {/* Khung filter theo giá */}
            <List
            size="md"
            variant="outlined"
            sx={{
                maxWidth: 200,
                borderRadius: 'md',
                mt: 2
            }}
            >
                <ListItem>
                    <ListItemButton>
                        <ListItemDecorator sx={{fontWeight: 'bold'}}>
                            Giá sách
                        </ListItemDecorator>
                    </ListItemButton>
                </ListItem>
                <ListItem>
                    <Input onChange={handlePriceMin} type="number" size="small" sx={{mr: 1}}>min</Input> 
                    {' - '}
                    <Input onChange={handlePriceMax} type="number" size="small" sx={{ml: 1}}>max</Input>
                </ListItem>
                {arrayListSale.map((value) => {
                    const labelId = `checkbox-list-secondary-label-${value}`
                    return(
                        <ListItem key={value}>
                            <Checkbox
                                edge="end"
                                onChange={handleToggle(value)}
                                checked={checked.indexOf(value) !== -1}
                                inputProps={{ 'aria-labelledby': labelId }}
                                sx={{mr: 1}}
                            ></Checkbox>
                            <ListItemButton>
                            <ListItemText id={labelId} primary={value} />
                            </ListItemButton>
                        </ListItem>
                    )
                })}
            </List>
            <Button sx={{
                mt: 3,
                ml: 5
            }} 
            onClick={handleClickFilter}
            variant='contained'
            >Lọc Sách</Button>
        </>
    )
}
export default FilterProduct;