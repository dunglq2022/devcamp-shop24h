import { Box } from "@mui/material";
import { Button } from "reactstrap";

function ViewAllProduct ({handleClickViewAllProps}) {
    return(
        <Box sx={{
            mt: 2,
            mb: 2
        }}>
            <Button onClick={handleClickViewAllProps} color="info">
                View All Products
            </Button>
        </Box>
    )
}

export default ViewAllProduct;