import { Button } from "@mui/material";
import { NavbarBrand } from "reactstrap";
function Logo () {
    return(
        <>
            <NavbarBrand href="/">
                <img
                alt="logo"
                src="https://reactstrap.github.io/logo-white.svg"
                style={{
                height: 40,
                width: 40
                }}
                />
                1980 Books
                <Button sx={{
                    marginLeft: 2
                }} variant="contained"
                href="/products"
                >Danh mục sản phẩm</Button>
            </NavbarBrand>
        </>
    )
}
export default Logo;