import { Navbar} from "reactstrap";
import Logo from "./Logo";
import IconNavbar from "./NavbarIcon";

function Header () {
    return (
        <Navbar
        className="my-2"
        color="dark"
        dark
        >
        <Logo/>
        <IconNavbar/>
        </Navbar>
    )
}
export default Header;