import { FaRegBell, FaRegCircleUser } from "react-icons/fa6";
import { BsCartCheck } from "react-icons/bs";
import { IconButton } from "@mui/material";
import { grey } from "@mui/material/colors";

function IconNavbar () {
    return(
        <>
            <div className="text-light me-5" style={{alignItems: 'right', display: 'flex'}}>
                <div className="me-3" style={{fontSize: 22}}><FaRegBell/></div>
                {/* <div className="me-3" style={{fontSize: 22}}><FaRegCircleUser/></div> */}
                <IconButton sx={{
                    color: grey[50],
                    mr: 2
                }}
                href="/login"
                ><FaRegCircleUser/></IconButton>
                <div style={{fontSize: 22}}><BsCartCheck/></div>
            </div>
        </>
    )
}
export default IconNavbar;