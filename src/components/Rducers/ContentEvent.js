const initialState = {
    arrProducts: [],
    filterProduct: []
}

const ContentEvent = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_PRODUCTS': 
            return {
                ...state,
                arrProducts: action.payload
            };

            case 'UPDATE_PRODUCTS' :
                return {
                    ...state,
                    filterProduct: action.payload
                }
        default: {
            return state
        }
    }
}

export default ContentEvent;