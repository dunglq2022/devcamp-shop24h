import { Col, NavbarBrand, Row } from "reactstrap";
import { BsFacebook, BsInstagram, BsYoutube, BsTwitter } from "react-icons/bs";

function SocialFooter () {
    return(
        <>
            <NavbarBrand href="/">
                <img
                    alt="logo"
                    src="https://reactstrap.github.io/logo-white.svg"
                    style={{
                    height: 40,
                    width: 40
                    }}
                />
            1980 Books
            </NavbarBrand>
            <Row>
                <Col>
                    <BsFacebook href="/Facebook" className="ms-3 me-2"/><BsInstagram href="/Instagram" className="me-2"/><BsYoutube href="/Youtube" className="me-2"/><BsTwitter href="/Twitter"/>
                </Col>
            </Row>
        </>
    )
}
export default SocialFooter;