import { Col, Row } from "reactstrap";
import ProductFoter from "./ProductFooter";
import ServiceFoter from "./ServicesFooter";
import SupportFoter from "./SupportFooter";
import SocialFooter from "./SocialFooter";

function Footer () {
    return(
        <div className="text-light text-center" style={{backgroundColor: '#212529'}}>
            <Row className="pt-5">
                <Col className="ps-5">
                    <Row xs={3}>
                        <Col>
                            <ProductFoter/>
                        </Col>
                        <Col>
                            <ServiceFoter/>
                        </Col>
                        <Col>
                            <SupportFoter/>
                        </Col>
                    </Row>
                </Col>
                <Col xs={4}>
                    <SocialFooter/>
                </Col>
            </Row>
        </div>
    )
}
export default Footer;