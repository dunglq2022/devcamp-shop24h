import LastestProduct from '../components/Contents/LastestProducts'
import Header from '../components/Header/Haeder';
import Footer from '../components/Footer/Footer';

function Product () {
    return(
        <>
        <Header/>
        <div className="container">
            <LastestProduct/>
        </div>
        <Footer/>
        </>
    )
}
export default Product;