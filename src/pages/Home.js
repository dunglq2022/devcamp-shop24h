import Contents from '../components/Contents/Contents'
import Footer from '../components/Footer/Footer';
import Header from '../components/Header/Haeder';

function Home() {
  return (
    <div>
      <Header/>
      <Contents/>
      <Footer/>
    </div>
  );
}

export default Home;