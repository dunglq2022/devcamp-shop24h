import { combineReducers, createStore } from "redux";
import contentEvent from '../components/Rducers/ContentEvent';
const appReducer = combineReducers({
    contentReducer: contentEvent
});

const store = createStore(
    appReducer,
);

export default store;